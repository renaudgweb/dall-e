# Dall-e

![Dall-e screenshot](public/static/screenshot.png)


## Description

Dall-e

Dall-e 

## API Key Configuration

Create a new file. Add the following line inside the file:

``` plaintext

OPENAI_API_KEY=sk-J6G...yourAPIkey
```
Save the file with the .env extension at the root of this project.


## Front demo

[link](https://renaudgweb.frama.io/dall-e/)


## Poof Of Concept

Install openai and dotenv.

`pip install openai`

`pip install python-dotenv`

Run the Python script.

`python3 app.py`

Watch to the generated 'output.png' file.


``` python
import os
import sys
from dotenv import load_dotenv
from openai import OpenAI
import base64

load_dotenv()
client = OpenAI(api_key=os.environ.get('OPENAI_API_KEY'))

response = client.images.generate(
    model="dall-e-2",
    prompt="A cute grey baby cat",
    n=1,
    size="1024x1024",
    response_format="b64_json"
)
image = response.data[0]
imgData = base64.b64decode(image.b64_json)

with open(f'output.png', 'wb') as f:
    f.write(imgData)


if __name__ == '__main__':
    try:
        app.run(use_reloader=False, debug=True)
    except KeyboardInterrupt:
        print('\n -> Interrupted')
        try:
            sys.exit(0)
        except SystemExit:
            os._exit(0)
```


## Test and Deploy

Use the built-in continuous integration in GitLab.

- [ ] [Get started with GitLab CI/CD](https://docs.gitlab.com/ee/ci/quick_start/index.html)
- [ ] [Analyze your code for known vulnerabilities with Static Application Security Testing (SAST)](https://docs.gitlab.com/ee/user/application_security/sast/)
- [ ] [Deploy to Kubernetes, Amazon EC2, or Amazon ECS using Auto Deploy](https://docs.gitlab.com/ee/topics/autodevops/requirements.html)
- [ ] [Use pull-based deployments for improved Kubernetes management](https://docs.gitlab.com/ee/user/clusters/agent/)
- [ ] [Set up protected environments](https://docs.gitlab.com/ee/ci/environments/protected_environments.html)

***


## LICENSE

MIT License

Copyright (c) 2023 RenaudG

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
