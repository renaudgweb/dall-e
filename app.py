#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import os
from pathlib import Path
from dotenv import load_dotenv
from flask import Flask, request, render_template, send_from_directory
from openai import OpenAI
import base64

# Charger les variables d'environnement depuis un fichier .env
project_folder = os.path.expanduser('~/dall-e')
load_dotenv(os.path.join(project_folder, '.env'))
client = OpenAI(api_key=os.environ.get('OPENAI_API_KEY'))

app = Flask(__name__)

# Définir le répertoire d'upload et les extensions autorisées
UPLOAD_FOLDER = Path(os.path.expanduser('~/dall-e/uploads'))
# OUTPUT = 'output.png'

app.config['UPLOAD_FOLDER'] = UPLOAD_FOLDER


@app.route('/', methods=['GET', 'POST'])
def generate_uploads():
    output_text_path = os.path.join(
        app.config['UPLOAD_FOLDER'],
        'user.txt'
    )
    with open(output_text_path, 'r') as f:
        user_input = f.read()

    if request.method == 'POST':
        user_input = request.form['user_input']
        # user_ratio = request.form['user_ratio']
        # user_quality = request.form['user_quality']
        # user_style = request.form['user_style']

        response = client.images.generate(
            model="dall-e-2",
            prompt=user_input,
            # size=user_ratio,
            # quality=user_quality,
            # n=1,
            n=4,
            # style=user_style,
            response_format="b64_json"
        )

#       image = response.data[0]
#       imgData = base64.b64decode(image.b64_json)

#       with open(output_text_path, 'w') as f:
#           f.write(user_input)

#       output_file_path = os.path.join(app.config['UPLOAD_FOLDER'], OUTPUT)
#       with open(output_file_path, 'wb') as f:
#           f.write(imgData)

#   return render_template('index.html',
#                          image_path=OUTPUT,
#                          user_input=user_input)

        for i, image_data in enumerate(response.data):
            img_data = base64.b64decode(image_data.b64_json)

            output_file_path = os.path.join(
                app.config['UPLOAD_FOLDER'],
                f"output_{i + 1}.png"
            )

            with open(output_file_path, 'wb') as f:
                f.write(img_data)

        with open(output_text_path, 'w') as f:
            f.write(user_input)

    return render_template('index.html',
                           image_path_1="output_1.png",
                           image_path_2="output_2.png",
                           image_path_3="output_3.png",
                           image_path_4="output_4.png",
                           user_input=user_input)


@app.route('/uploads/<filename>')
def uploaded_file(filename):
    return send_from_directory(app.config['UPLOAD_FOLDER'], filename)


if __name__ == '__main__':
    # Créer le répertoire d'upload s'il n'existe pas
    if not UPLOAD_FOLDER.exists():
        UPLOAD_FOLDER.mkdir()
    # Créer le fichier user.txt s'il n'existe pas
    user_txt_path = os.path.join(UPLOAD_FOLDER, 'user.txt')
    if not Path(user_txt_path).exists():
        Path(user_txt_path).touch()

    app.run()
