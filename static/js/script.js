let pix = document.getElementsByClassName("pixel");

for (let i = 0; i < pix.length; i++) {
  pix[i].style.animationDelay = Math.ceil(Math.random()*5000)+"ms";
}

// let imgOutput = document.getElementById('output');

// imgOutput.addEventListener('load', function() {
//   imgOutput.style.display = 'block';
// });

// Afficher l'image une fois qu'elle est entièrement chargée
function handleImageLoad(imgOutput) {
  imgOutput.addEventListener('load', function() {
    imgOutput.style.display = 'block';
  });
}

let imgOutput1 = document.getElementById('output_1');
let imgOutput2 = document.getElementById('output_2');
let imgOutput3 = document.getElementById('output_3');
let imgOutput4 = document.getElementById('output_4');

handleImageLoad(imgOutput1);
handleImageLoad(imgOutput2);
handleImageLoad(imgOutput3);
handleImageLoad(imgOutput4);
